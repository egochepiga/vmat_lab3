program lab3
    external RKF45

    integer:: IWORK(5), nstep, step, i, i2, num = 2, iflag = 1
    real:: WORK(15), dx(2), x(2) = [0, 1]
    real:: h_print = 0.02
    real:: relerr = 1e-4
    real:: abserr = 1e-4
    real:: temp, tn = 0.4
    character(45) :: format = "(' |' f12.4 '  |' f20.3 '  |' f20.5 '    |')"

    write(*,*) "----------------------------------------------------------------"
    write(*,*) "|      t       |              ", "x(1)    |               ", "x(2)     |"
    write(*,*) "----------------------------------------------------------------"

    nstep = tn/h_print
    do i=1, nstep
            t = h_print * (i-1)
            tout = h_print * i
        CALL RKF45(rkf_fun, num, x, t, tout, relerr, abserr, iflag, WORK, IWORK)

        if (iflag /= 2) then !Проверка ошибки в работе RKF45
            write(*,*) "Error in rkf45? iflag"
            exit
        end if

        write(*,format) t, x(1), x(2)
    end do

    write(*,*) "----------------------------------------------------------------"
    write(*,*)
    write(*,*) "----------------------------------------------------------------"
    write(*,*) "|      t       |              ", "x(1)    |               ", "x(2)     |"
    write(*,*) "----------------------------------------------------------------"

    x(1) = 0;
    x(2) = 1;

    do i2=1, nstep
        t = h_print * (i2-1)
        tout = h_print * i2
        temp = rk_2(2, x, t, tout, h_print)
        write(*,format) t, x(1), x(2)
    end do

    write(*,*) "----------------------------------------------------------------"
    write(*,*)
    write(*,*) "----------------------------------------------------------------"
    write(*,*) "|      t       |              ", "x(1)    |               ", "x(2)     |"
    write(*,*) "----------------------------------------------------------------"

    step = 0
    h_print = 0.01
    nstep = tn/h_print
    x(1) = 0;
    x(2) = 1;

    do i2=1, nstep
        step = step + 1
        t = h_print * (i2-1)
        tout = h_print * i2
        temp = rk_2(2, x, t, tout, h_print)
        if (step == 2) then
            step = 0
            write(*,format) t, x(1), x(2)
        end if
    end do
    write(*,*) "----------------------------------------------------------------"

contains

    SUBROUTINE rkf_fun(t, x, dx)
        integer :: y(2), n
        real:: t, x(2), dx(2)
        dx(1) =  -310 * x(1) - 3000 * x(2) + 1 / (10*t*t + 1)
        dx(2) = x(1) + exp(-2*t)
    end

    function rk_2(n, x, t, t_out, h_int) result(res)
        integer :: n
        real :: t, t_out, h_int, temp1(n), temp2(n), preresult(n), x(n), tmp(2)


            call rkf_fun(t, x, temp1)
            do i = 1, n
                preresult(i) = x(i) + 2/3 * h_int * temp1(i)
            end do

            call rkf_fun(t +  h_int * 2/ 3, preresult, temp2)
            do i = 1, n
                x(i) = x(i) + (temp1(i) + 3 * temp2(i)) * h_int / 4;
            end do

            t = t + h_int
    end function rk_2

end program lab3